from sqlalchemy import Integer, Column
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Cave(Base):
    id = Column(Integer, primary_key=True, index=True)
    size = Column(Integer, nullable=False)
    travelers = relationship("Traveler", cascade="all, delete-orphan",
                             back_populates="cave", uselist=True)
