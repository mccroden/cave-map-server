from sqlalchemy import Integer, Column, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Treasure(Base):
    id = Column(Integer, primary_key=True, index=True)
    square = Column(Integer, ForeignKey("square.loc"), nullable=True)
    type = Column(Integer, nullable=False)
    modified_dt = Column(DateTime, nullable=False)
