from sqlalchemy import Integer, Column, DateTime
from sqlalchemy.dialects.postgresql import JSONB

from app.db.base_class import Base


class Request(Base):
    id = Column(Integer, primary_key=True, index=True)
    received_dt = Column(DateTime, nullable=False)
    request_dump = Column(JSONB, nullable=False)
