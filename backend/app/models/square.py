from sqlalchemy import Integer, String, Column, DateTime
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Square(Base):
    loc = Column(Integer, primary_key=True, index=True)
    x = Column(Integer, nullalbe=False)
    y = Column(Integer, nullalbe=False)
    grid_size(Integer, nullable=False)
    sprite = Column(String(256), nullable=False)
    movement = Column(Integer, nullable=True)
    object_num = Column(Integer, nullable=True)
    object_id = Column(Integer, nullable=True)
    modified_dt = Column(DateTime, nullable=False)
    treasure = relationship("Treasure", cascade="all, delete-orphan",
                            back_populates="treasure", uselist=True)
