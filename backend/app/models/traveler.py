from sqlalchemy import Float, Integer, Column, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base

class Traveler(Base):
    id = Column(Integer, primary_key=True, index=True)
    loc = Column(Integer, nullable=False)
    step_count = Column(Integer, nullable=False)
    inventory_1 = Column(Integer, nullable=True)
    inventory_2 = Column(Integer, nullable=True)
    inventory_3 = Column(Integer, nullable=True)
    energy = Column(Float, nullable=True)
    wool = Column(Integer, nullable=True)
    cave_id = Column(Integer, ForeignKey("cave.id"), nullable=True)
    cave = relationship("Cave", back_populates="travelers")
