from typing import Optional
from datetime import datetime

import databases
import sqlalchemy
import urllib
import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

#DATABASE_URL = "postgresql://localhost:5432"
DATABASE_URL = "postgresql://localhost:5432/caveMap"

database = databases.Database(DATABASE_URL)

metadata = sqlalchemy.MetaData()

app = FastAPI()

# frontend, react, will run on port 3000
origins = ["http://localhost:3000", "localhost:3000"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

notes = sqlalchemy.Table(
    "squares",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("modify_dt", sqlalchemy.Integer),
    sqlalchemy.Column("loc", sqlalchemy.Integer),
    sqlalchemy.Column("pack", sqlalchemy.Integer),
    sqlalchemy.Column("x", sqlalchemy.Integer),
    sqlalchemy.Column("y", sqlalchemy.Integer),
    sqlalchemy.Column("sprite", sqlalchemy.Integer),
    sqlalchemy.Column("movement", sqlalchemy.Integer),
    sqlalchemy.Column("source_object_num", sqlalchemy.Integer),
    sqlalchemy.Column("source_object_id", sqlalchemy.Integer),
)


notes = sqlalchemy.Table(
    "traveler",
    metadata,
    sqlalchemy.Column("token_id", sqlalchemy.Integer),
    sqlalchemy.Column("pack", sqlalchemy.Integer),
    sqlalchemy.Column("loc", sqlalchemy.Integer),
    sqlalchemy.Column("x", sqlalchemy.Integer),
    sqlalchemy.Column("y", sqlalchemy.Integer),
    sqlalchemy.Column("dug", sqlalchemy.Integer),
    sqlalchemy.Column("step_count", sqlalchemy.Integer),
    sqlalchemy.Column("inventory_1", sqlalchemy.Integer),
    sqlalchemy.Column("inventory_2", sqlalchemy.Integer),
    sqlalchemy.Column("inventory_3", sqlalchemy.Integer),
)

engine = sqlalchemy.create_engine(
    DATABASE_URL, connect_args={}
)

metadata.create_all(engine)


class Square(BaseModel):
    modify_dt: datetime
    loc: int
    pack: int
    x: int
    y: int
    sprite: int
    movement: int
    source_object_num: int | None = None
    source_object_id: int | None = None


class Taveler(BaseModel):
    token_id: int
    pack: int
    loc: int
    x: int
    y: int
    dug: bool
    step_count: int
    inventory_1: int
    inventory_2: int
    inventory_3: int


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

# @app.get("/", tags=["root"])
# async def read_root() -> dict:
#    return {"message": "Welcome to your todo list."}


@app.get("/map/{cave_id}")
def read_map(cave_id: int, x: Optional[int] = 0, y: Optional[int] = 0):
    return {"cave_id": cave_id, "x": x, "y": y}
